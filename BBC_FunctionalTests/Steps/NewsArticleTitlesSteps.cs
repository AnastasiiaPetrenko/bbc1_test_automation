﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using TechTalk.SpecFlow;
using BBC_FunctionalTests.PageObjects;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using BBC_FunctionalTests.Utils;

namespace BBC_FunctionalTests.Steps
{
    [Binding]
    public class NewsArticleTitlesSteps
    {
        IWebDriver driver = WebDriver.Driver;

        [AfterScenario]
        public void CloseDriver()
        {
            driver.Quit();
        }

        [Given(@"The user navigate to URL")]
        public void GivenTheUserNavigateToURL()
        {
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl("https://www.bbc.com");
        }

        [When(@"The user goes to the news page")]
        public void WhenTheUserGoesToTheNewsPage()
        {
            new HomePage(driver).ClickTheNewsButton();
            new NewsPage(driver).CloseTheSignInPopUp();
        }

        [Then(@"The user can verify that topStory title matches the '(.*)'")]
        public void ThenTheUserCanVerifyThatTopStoryTitleMatchesThe(string title)
        {
            NewsPage newsPage = new NewsPage(driver);
            Assert.AreEqual(title, newsPage.GetTopArticleStoryTitleText());
        }

        [Then(@"The user can verify that secondary titles matches the")]
        public void ThenTheUserCanVerifyThatSecondaryTitlesMatchesThe(Table table)
        {
            List<string> values = TableExtensions.ToList(table);
            NewsPage newsPage = new NewsPage(driver);
            List<IWebElement> titlesList = newsPage.GetSecondaryTitlesList();
            for (int i = 0; i < titlesList.Count; i++)
                Assert.AreEqual(values[i], newsPage.GetTextFromIWebElement(titlesList[i]));
        }

        [When(@"The user enter category of topStoryTitle in the search bar")]
        [Obsolete("Creation of inner variable")]
        public void WhenTheUserEnterCategoryOfTopStoryTitleInTheSearchBar()
        {
            NewsPage news = new NewsPage(driver);
            string search = news.GetTextFromCategoryTitle();
            news.SearchByCategoryOnNewsPage();

            ScenarioContext.Current["Search"] = search;

        }

        [Then(@"The user can verify that first news title contains entered word")]
        public void ThenTheUserCanVerifyThatFirstNewsTitleContainsEnteredWord()
        {
            var search = (string)ScenarioContext.Current["Search"];
            string actualTitle = new SearchResultsPage(driver).GetTextFromFirstArticleHeading();

            Assert.IsTrue(actualTitle.Contains(search));
        }

    }
}
