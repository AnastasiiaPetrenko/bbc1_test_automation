﻿using TechTalk.SpecFlow;
using BBC_FunctionalTests.PageObjects;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using BBC_FunctionalTests.Utils;
using System.Linq;

namespace BBC_FunctionalTests.Steps
{
    [Binding]
    public class QuestionFormSteps
    {

        [Given(@"The user opens the form")]
        public void GivenTheUserOpensTheForm()
        {
            WebDriver.Driver.Manage().Window.Maximize();
            WebDriver.Driver.Navigate().GoToUrl("https://www.bbc.com");
            new HomePage(WebDriver.Driver).ClickTheNewsButton();
            NewsPage news = new NewsPage(WebDriver.Driver);
            news.CloseTheSignInPopUp();
            news.GoToCoronavirusPandemicPage();
            new CoronavirusPage(WebDriver.Driver).GoToCoronavirusStoriesPage();
            new CoronavirusStoriesPage(WebDriver.Driver).GoToWhatQuestionsDoYouHavePage();
            new WhatQuestionsDoYouHave(WebDriver.Driver).GoToForm();
        }

        [When(@"The user submit the form with values")]
        public void WhenTheUserSubmitTheFormWithValues(Table table)
        {
            Dictionary<string, string> values = TableExtensions.ToDictionary(table);
            new QuestionForm(WebDriver.Driver).SubmitForm(values);
        }


        [When(@"The user submit the form with data values")]
        public void WhenTheUserSubmitTheFormWithNewValues(Table table)
        {
            Dictionary<string, string> values = table.Rows.ToDictionary(key => key.Values.ElementAt(0), value => value.Values.ElementAt(1));
            new QuestionForm(WebDriver.Driver).SubmitForm(values);
        }


        [Then(@"The user verify that (.*) error message\(s\) is displayed")]
        public void ThenTheUserVerifyThatErrorMessage(string err)
        {
            Assert.AreEqual(err, new QuestionForm(WebDriver.Driver).GetErrorMessage());
        }

    }
}
