﻿Feature: NewsArticleTitles
	As a developer
	I want to check the articles title on the news page
	In order to verify that news are up-to-date

@mytag
Scenario Outline: Check the topStory title  
	Given The user navigate to URL
	When The user goes to the news page
	Then The user can verify that topStory title matches the 'US coronavirus death toll passes 200,000'

Scenario Outline: Check the secondary article titles  
	Given The user navigate to URL
	When The user goes to the news page
	Then The user can verify that secondary titles matches the 
	| Titles                                             |
	| Trump says Supreme Court nominee will be a woman   |
	| Judge blocks US attempts to ban China's WeChat     |
	| Sarcophagi buried for 2,500 years exhumed in Egypt |
	| Flypast marks Battle of Britain anniversary        |
	| Runner gives up medal to rival who went wrong way  |

Scenario Outline: Check the name of article after search by category  
	Given The user navigate to URL
	When The user goes to the news page
	And The user enter category of topStoryTitle in the search bar 
	Then The user can verify that first news title contains entered word