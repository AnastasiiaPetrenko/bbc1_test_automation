﻿Feature: AskQuestionToBBCForm
	In order to check the behavior of form
	As a tester
	I want to run negative tests about submitting form with various data

@mytag
Scenario Outline: Submit form with invalid data 
	Given The user opens the form
	When The user submit the form with data values
	| Key                        | Value        |
	| Let us know your question. | <story>      |
	| Name                       | <name>       |
	| Email address              | <email>      |
	| Age                        | <age>        |
	| Postcode                   | <code>       |
	| Telephone number           | <phone>      |
	| ageAbove16                 | <ageAbove16> |
	| acceptTermsOfService       | <terms>      |
	Then The user verify that <errors> error message(s) is displayed

Examples:
| story                    | name       | email                        | age | code  | phone | ageAbove16 | terms | errors |
| "Here it is my question" | Anastasiia | anastasi.petrenko9@gmail.com | 20  | 02094 | 10    | Yes        | No    | 1      |
|                          |            |                              | 20  | 02094 |       | Yes        | No    | 5      |
|                          |            |                              | 20  | 02094 |       | No         | No    | 6      |
