﻿using BBC_FunctionalTests.PageObjects;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace BBC_FunctionalTests
{
    class BusinessLogicLayer
    {
        private readonly IWebDriver driver;

        public BusinessLogicLayer(IWebDriver driver)
        {
            this.driver = driver;
            PageFactory.InitElements(driver, this);
        }

        public void AskQuestionToBBC(Dictionary<string, string> values)
        {
            new HomePage(driver).ClickTheNewsButton();
            NewsPage news = new NewsPage(driver);
            news.CloseTheSignInPopUp();
            news.GoToCoronavirusPandemicPage();
            new CoronavirusPage(driver).GoToCoronavirusStoriesPage();
            new CoronavirusStoriesPage(driver).GoToWhatQuestionsDoYouHavePage();
            QuestionForm form = new WhatQuestionsDoYouHave(driver).GoToForm();

            form.SubmitForm(values);
        }
    }
}
