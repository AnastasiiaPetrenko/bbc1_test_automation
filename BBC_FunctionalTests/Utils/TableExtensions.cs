﻿using System.Collections.Generic;
using TechTalk.SpecFlow;

namespace BBC_FunctionalTests.Utils
{
    public class TableExtensions
    {
        public static Dictionary<string, string> ToDictionary(Table table)
        {
            var dictionary = new Dictionary<string, string>();
            foreach (var row in table.Rows)
            {
                dictionary.Add(row[0], row[1]);
            }
            return dictionary;
        }

        public static List<string> ToList(Table table)
        {
            var list = new List<string>();
            foreach (var row in table.Rows)
            {
                list.Add(row[0]);
            }
            return list;
        }

        //public static Dictionary<string, string> ToDictionaryFrom2Lists(Table table)
        //{
        //    IEnumerable<string> keys = new List<string>() { "NumberOfTests", "Let us know your question.", "Name", "Email address", "Age", "Postcode", "Telephone number", "ageAbove16", "acceptTermsOfService", "errors" };
        //    IEnumerable<string> values = (IEnumerable<string>)table;
        //    var dic = keys.Zip(values, (k, v) => new { k, v }).ToDictionary(x => x.k, x => x.v);

        //    return (Dictionary<string, string>)dic.Skip(1).Take(8);
        //}
    }
}
