﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BBC_FunctionalTests
{    
    class QuestionForm
    {
        private readonly IWebDriver driver;

        [FindsBy(How = How.XPath, Using = "//button[text()='Submit']")]
        private IWebElement SubmitButton { get; set; }

        public QuestionForm(IWebDriver driver)
        {
            this.driver = driver;
            PageFactory.InitElements(driver, this);
        }

        public void SubmitForm(Dictionary<string, string> values)
        {
            List<string> keys = values.Keys.Take(8).ToList();
            
            foreach (string key in values.Keys)
            {
                if(values[key] == null)
                {
                    values[key] = " ";
                }
                string path = "";
                if(keys.Take(6).Any(k => k == key)) {
                    path = "//*[@aria-label='" + key + "']";
                    driver.FindElement(By.XPath(path)).SendKeys(values[key]);
                }

                if (values[key] == "Yes")
                {
                    int number = (keys.IndexOf(key) + 1) % 6;
                    path = "//div[contains(@id, 'module-4787')]//div[@class='checkbox'][" + number + "]/label";
                    driver.FindElement(By.XPath(path)).Click();
                }
                
            }
            SubmitButton.Click();
        }
        
        public string GetErrorMessage()
        {
            List<IWebElement> errors = driver.FindElements(By.XPath("//div[contains(@class, 'input-error')]")).ToList<IWebElement>();
            return errors.Count.ToString();
        }
    }
}
