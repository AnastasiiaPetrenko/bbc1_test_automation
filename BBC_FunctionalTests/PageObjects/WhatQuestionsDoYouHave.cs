﻿using OpenQA.Selenium;

namespace BBC_FunctionalTests.PageObjects
{
    class WhatQuestionsDoYouHave : BasePage
    {
        public WhatQuestionsDoYouHave(IWebDriver driver) : base(driver)
        {
            
        }

        public QuestionForm GoToForm()
        {
            return new QuestionForm(driver);
        }
    }
}
