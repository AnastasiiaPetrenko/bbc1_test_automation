﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace BBC_FunctionalTests.PageObjects
{
    class BasePage
    {
        protected IWebDriver driver;

        public BasePage(IWebDriver driver)
        {
            this.driver = driver;
            PageFactory.InitElements(driver, this);
        }

        public IWebElement GetIWebElementByTextAndNumber(string label, int number)
        {
            By path = By.XPath("//a[contains(text(), '" + label + "')][" + number + "]");
            return driver.FindElement(path);
        }
        public void ImplicitWait(int time)
        {
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(time);
        }

        public void WaitForPageLoad(int time)
        {
            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(time);
        }
    }
}
