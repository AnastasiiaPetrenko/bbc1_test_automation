﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;

namespace BBC_FunctionalTests.PageObjects
{
    class NewsPage : BasePage
    {
        public NewsPage(IWebDriver driver) : base(driver)
        {
            
        }

        [FindsBy(How = How.XPath, Using = "//div[@data-entityid = 'container-top-stories#1']/div[1]//h3")]
        private IWebElement TopArticleStoryTitle { get; set; }

        [FindsBy(How = How.XPath, Using = "//button[@class='sign_in-exit']")]
        private IWebElement MaybeLatterButton { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@data-entityid='container-top-stories#1']/div[1]//a[contains(@class, 'section')]")]
        private IWebElement CategoryOfStory { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@id='orb-search-q']")]
        private IWebElement SearchInput { get; set; }

        [FindsBy(How = How.XPath, Using = "//button[@id='orb-search-button']")]
        private IWebElement SearchButton { get; set; }
        
        [FindsBy(How = How.XPath, Using = "//ul[contains(@class, 'border nw')]//a[./span[contains(text(), 'Coronavirus')]]")]
        private IWebElement CoronavirusLink { get; set; }

        public void CloseTheSignInPopUp()
        {
            if (MaybeLatterButton.Displayed)
                MaybeLatterButton.Click();
        }

        public string GetTopArticleStoryTitleText()
        {
            return TopArticleStoryTitle.Text;
        }

        public List<IWebElement> GetSecondaryTitlesList()
        {
            By path = By.XPath("//div[contains(@class, 'secondary-item')]//h3");
            return driver.FindElements(path).ToList<IWebElement>();
        }

        public string GetTextFromIWebElement(IWebElement element)
        {
            return element.Text;
        }

        public string GetTextFromCategoryTitle()
        {
            return CategoryOfStory.Text;
        }

        public void SearchByCategoryOnNewsPage()
        {
            WaitForPageLoad(15);
            SearchInput.SendKeys(GetTextFromCategoryTitle());
            SearchButton.Click();
        }

        public void GoToCoronavirusPandemicPage()
        {
            WaitForPageLoad(15);
            CoronavirusLink.Click();
        }
    }
}
