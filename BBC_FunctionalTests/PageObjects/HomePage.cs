﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace BBC_FunctionalTests.PageObjects
{
    class HomePage : BasePage
    {
        public HomePage(IWebDriver driver) : base(driver)
        {
            
        }

        public void ClickTheNewsButton()
        {
            ImplicitWait(15);
            GetIWebElementByTextAndNumber("News", 1).Click();
        }
    }
}
