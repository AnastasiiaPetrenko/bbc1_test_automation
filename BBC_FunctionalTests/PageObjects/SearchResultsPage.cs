﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace BBC_FunctionalTests.PageObjects
{
    class SearchResultsPage : BasePage
    {
        public SearchResultsPage(IWebDriver driver) : base(driver)
        {
            
        }

        [FindsBy(How = How.XPath, Using = "//ul[@class = 'css-1lb37cz-Stack e1y4nx260']/li[1]//div[contains(@class, 'PromoContent')]//a/span")]
            private IWebElement FirstArticleHeading { get; set; }

        public string GetTextFromFirstArticleHeading()
        {
            return FirstArticleHeading.Text;
        }
    }
}
