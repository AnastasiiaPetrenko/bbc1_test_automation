﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace BBC_FunctionalTests.PageObjects
{
    class CoronavirusPage : BasePage
    {
        [FindsBy(How = How.XPath, Using = "//ul[contains(@class, 'secondary-sections')]//a[@href='/news/have_your_say']")]
        private IWebElement yourCoronavirusStoriesLink { get; set; }

        public CoronavirusPage(IWebDriver driver) : base (driver)
        {
            
        }

        public CoronavirusStoriesPage GoToCoronavirusStoriesPage()
        {
            WaitForPageLoad(20);
            yourCoronavirusStoriesLink.Click();
            return new CoronavirusStoriesPage(driver);
        }

    }
}
