﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace BBC_FunctionalTests.PageObjects
{
    class CoronavirusStoriesPage : BasePage
    {
        [FindsBy(How = How.XPath, Using = "//a[./h3[contains(text(), 'Send us your questions')]]")]
        private IWebElement SendUsYourQuestionLink { get; set; }

        public CoronavirusStoriesPage(IWebDriver driver) : base(driver)
        {
            
        }

        public WhatQuestionsDoYouHave GoToWhatQuestionsDoYouHavePage()
        {
            WaitForPageLoad(15);
            SendUsYourQuestionLink.Click();
            return new WhatQuestionsDoYouHave(driver);
        }
    }
}
